package cz.cvut.fit.vizeldim.controller;

import cz.cvut.fit.vizeldim.dto.create.TagCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.TagDTO;
import cz.cvut.fit.vizeldim.entity.Post;
import cz.cvut.fit.vizeldim.entity.Tag;
import cz.cvut.fit.vizeldim.service.PostService;
import cz.cvut.fit.vizeldim.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/tags")
public class TagController extends MyController<Tag, TagDTO, TagCreateDTO>{
    private final TagService tagService;

    private final PostService postService;

    @Autowired
    public TagController(TagService tagService, PostService postService) { this.tagService = tagService;
        this.postService = postService;
    }

    @Override
    protected TagService getService() { return tagService; }

    @GetMapping(params = {"post"})
    @ResponseStatus(HttpStatus.OK)
    public List<TagDTO> readAllTagsInPost(@RequestParam long post)
    {
        Optional<Post> optionalPost = postService.findByIdAsEntity(post);

        if (optionalPost.isPresent()) {
            return getService()
                    .findAllFromPost(optionalPost.get());
        }
        else
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<TagDTO> readAll()
    {
        return getService().findAll();
    }
}
