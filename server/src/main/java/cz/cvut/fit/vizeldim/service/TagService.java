package cz.cvut.fit.vizeldim.service;

import cz.cvut.fit.vizeldim.dto.create.TagCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.TagDTO;
import cz.cvut.fit.vizeldim.entity.Post;
import cz.cvut.fit.vizeldim.entity.Tag;
import cz.cvut.fit.vizeldim.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class TagService extends MyService<Tag, TagDTO, TagCreateDTO>
{
    private final TagRepository tagRepository;

    @Autowired
    public TagService(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public TagRepository getRep() {
        return tagRepository;
    }

    @Override
    public TagDTO toDTO(Tag entity) {
        return new TagDTO(
                entity.getId(),
                entity.getName()
        );
    }

    @Override
    public Optional<Tag> toEntity(TagCreateDTO createDTO) {
        if (isValid(createDTO)){
            return Optional.of(
                    new Tag(createDTO.getName(),
                            Collections.emptyList())
            );
        }

        return Optional.empty();
    }

    @Override
    public Tag updateEntity(Tag entity, TagCreateDTO createDTO) {
        if (createDTO.getName()!=null)
            entity.setName(createDTO.getName());
        return entity;
    }

    @Override
    public boolean isEmpty(TagCreateDTO createDTO) {
        return createDTO.getName()==null;
    }

    @Override
    public boolean isValid(TagCreateDTO createDTO) {
        return createDTO.getName()!=null;
    }

    @Override
    protected Optional<Long> getId(Tag entity) {
        if (entity.getId() == null)
            return Optional.empty();
        return Optional.of(entity.getId());
    }


    public List<TagDTO> findAllFromPost(Post post){
        return getRep()
                .findAllByPostsContains(post)
                .stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }


    public Set<Tag> findByIdsAsEntities(Set<Long> ids){
        Set<Tag> tags = new HashSet<>();

        ids.forEach( (id)-> {Optional<Tag> tag = findByIdAsEntity(id);
                             tag.ifPresent(tags::add);} );

        return tags;
    }


    public List<TagDTO> findAll() {
        return getRep().findAll()
                .stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }
}