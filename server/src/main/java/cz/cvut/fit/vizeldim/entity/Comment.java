package cz.cvut.fit.vizeldim.entity;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Objects;

@Entity
public class Comment{
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String content;

    @NotNull
    private final LocalDate added;

    @NotNull
    @ManyToOne
    private Post toPost;

    public Comment(String content, Post toPost) {
        this.content = content;
        this.added = LocalDate.now();
        this.toPost = toPost;
    }

    public Comment() {
        this.added = LocalDate.now();
    }

    public Long getId() { return id; }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDate getAdded() {
        return added;
    }

    public Post getToPost() {
        return toPost;
    }

    public void setToPost(Post toPost) {
        this.toPost = toPost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return Objects.equals(id, comment.id) &&
                Objects.equals(content, comment.content) &&
                Objects.equals(added, comment.added) &&
                Objects.equals(toPost, comment.toPost);
    }
}
