package cz.cvut.fit.vizeldim.service;

import cz.cvut.fit.vizeldim.dto.create.PostCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.PostDTO;
import cz.cvut.fit.vizeldim.entity.Category;
import cz.cvut.fit.vizeldim.entity.Post;
import cz.cvut.fit.vizeldim.entity.Tag;
import cz.cvut.fit.vizeldim.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;

@Service
public class PostService extends MyService<Post, PostDTO, PostCreateDTO>
{
    private final PostRepository postRepository;
    private final CategoryService categoryService;

    @Autowired
    public PostService(PostRepository postRepository,
                       CategoryService categoryService) {
        this.postRepository = postRepository;
        this.categoryService = categoryService;
    }

    @Override
    public PostRepository getRep() {
        return postRepository;
    }

    @Override
    public PostDTO toDTO(Post entity) {
        return new PostDTO(
                entity.getId(),
                entity.getTitle(),
                entity.getContent(),
                entity.getCreated(),
                entity.getCategory().getId()
                );
    }

    @Override
    public Optional<Post> toEntity(PostCreateDTO createDTO) {
        if (isValid(createDTO)){
            Optional<Category> optionalCategory = categoryService.findByIdAsEntity(createDTO.getCategoryId());
            return optionalCategory.map(category -> new Post(createDTO.getTitle(),createDTO.getContent(),category));
        }

        return Optional.empty();
    }

    @Override
    public Post updateEntity(Post entity, PostCreateDTO createDTO) {
        if (createDTO.getTitle()!=null)
            entity.setTitle(createDTO.getTitle());

        if (createDTO.getContent()!=null)
            entity.setContent(createDTO.getContent());

        if (createDTO.getCategoryId()!=null){
            Optional<Category> optionalCategory = categoryService.findByIdAsEntity(createDTO.getCategoryId());
            optionalCategory.ifPresent(entity::setCategory);
        }

        return entity;
    }

    @Override
    public boolean isEmpty(PostCreateDTO createDTO) {
        return  createDTO.getCategoryId()==null &&
                createDTO.getTitle()==null &&
                createDTO.getContent()==null;
    }

    @Override
    public boolean isValid(PostCreateDTO createDTO) {
        return  createDTO.getTitle()!=null &&
                createDTO.getContent()!=null &&
                createDTO.getCategoryId()!=null;
    }

    @Override
    protected Optional<Long> getId(Post entity) {
        if (entity.getId() == null)
            return Optional.empty();
        return Optional.of(entity.getId());
    }

    public Optional<Page<PostDTO>> findPageFromCategory(Pageable page, long categoryId) {
        Optional<Category> optionalCategory = categoryService.findByIdAsEntity(categoryId);

        if (optionalCategory.isPresent())
        {
            Page<PostDTO> found = getRep()
                    .findAllByCategory(page,optionalCategory.get())
                    .map(this::toDTO);

            return Optional.of(found);
        }
        else
            return Optional.empty();
    }

    public Page<PostDTO> findPageWithTag(Pageable page, Tag tag) {
        return getRep()
                .findAllByTagsContains(page,tag)
                .map(this::toDTO);
    }

    @Transactional
    public void addTags(Post post, Set<Tag> tagIds){
        post.getTags().addAll(tagIds);
    }

    @Transactional
    public void removeTags(Post post, Set<Tag> tagIds){
        post.getTags().removeAll(tagIds);
    }
}
