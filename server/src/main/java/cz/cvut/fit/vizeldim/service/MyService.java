package cz.cvut.fit.vizeldim.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

//E = Entity
//D = DTO
//C = createDTO
@Service
public abstract class MyService<E, D, C> {

    /*--------- Helpers -----------*/
    public abstract JpaRepository<E,Long> getRep        ();
    public abstract D                     toDTO         (E entity);
    public abstract Optional<E>           toEntity      (C createDTO);
    public abstract E                     updateEntity  (E entity,
                                                         C createDTO);

    public abstract boolean isEmpty(C createDTO);
    public abstract boolean isValid(C createDTO);

    private Optional<D> toDTO(Optional<E> optionalEntity) {
        if (optionalEntity.isEmpty())
            return Optional.empty();
        return Optional.of(toDTO(optionalEntity.get()));
    }

    protected abstract Optional<Long> getId(E entity);

    /*---------- CREATE -------------*/
    @Transactional
    public Optional<Long> create(C createDTO) {
        Optional<E> entity = toEntity(createDTO);
        if(entity.isPresent())
            return getId(getRep().save(entity.get()));
        else
            return Optional.empty();
    }

    /*---------- READ ---------------*/
    public Optional<E> findByIdAsEntity(Long id) {
        return getRep().findById(id);
    }

    public Optional<D> findByIdAsDTO(Long id) {
        return toDTO(getRep().findById(id));
    }

    public Page<D> findPage(Pageable page) {
        return getRep()
                .findAll(page)
                .map(this::toDTO);
    }


    /*---------- UPDATE --------------*/
    @Transactional
    public boolean update(Long id, C createDTO){
        Optional<E> optionalE = getRep().findById(id);
        if (optionalE.isEmpty())
            return false;

        E entity = optionalE.get();
        entity = updateEntity(optionalE.get(),createDTO);
        return true;
    }

    /*--------- DELETE ---------------*/
    @Transactional
    public void deleteById(Long id) { getRep().deleteById(id); }
}