package cz.cvut.fit.vizeldim.dto.create;

import java.util.Objects;

public class CategoryCreateDTO {
    protected final String name;

    protected final String description;

    public CategoryCreateDTO(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() { return name; }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryCreateDTO that = (CategoryCreateDTO) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(description, that.description);
    }

}