package cz.cvut.fit.vizeldim.repository;

import cz.cvut.fit.vizeldim.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category,Long> {
}
