package cz.cvut.fit.vizeldim.service;

import cz.cvut.fit.vizeldim.dto.create.CategoryCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.CategoryDTO;
import cz.cvut.fit.vizeldim.entity.Category;

import cz.cvut.fit.vizeldim.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CategoryService extends MyService< Category,
                                                CategoryDTO,
                                                CategoryCreateDTO >
{
    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public CategoryRepository getRep() {
        return this.categoryRepository;
    }

    @Override
    public CategoryDTO toDTO(Category entity) {
        return new CategoryDTO(
                entity.getId(),
                entity.getName(),
                entity.getDescription());
    }

    @Override
    public Optional<Category> toEntity(CategoryCreateDTO createDTO) {
        if (isValid(createDTO)) {
            return Optional.of(
                    new Category(createDTO.getName(),
                                 createDTO.getDescription())
            );
        }

        return Optional.empty();
    }

    @Override
    public Category updateEntity(Category entity, CategoryCreateDTO createDTO) {
        if (createDTO.getName()!=null)
            entity.setName(createDTO.getName());
        if (createDTO.getDescription()!=null)
            entity.setDescription(createDTO.getDescription());
        return entity;
    }

    @Override
    public boolean isEmpty(CategoryCreateDTO createDTO) {
        return  createDTO.getName()==null &&
                createDTO.getDescription()==null;
    }

    @Override
    public boolean isValid(CategoryCreateDTO createDTO) {
        return createDTO.getName()!=null;
    }

    @Override
    protected Optional<Long> getId(Category entity) {
        if (entity.getId() == null)
            return Optional.empty();
        return Optional.of(entity.getId());
    }


    public List<CategoryDTO> findAll() {
        return getRep().findAll()
                .stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

}
