package cz.cvut.fit.vizeldim.dto.response;

import cz.cvut.fit.vizeldim.dto.create.PostCreateDTO;
import java.time.LocalDate;
import java.util.Objects;

public class PostDTO extends PostCreateDTO {
    private final Long id;

    private final LocalDate created;

    public PostDTO(Long id,
                   String title,
                   String content,
                   LocalDate created,
                   Long categoryId)
    {
        super(title,content,categoryId);
        this.created = created;
        this.id = id;
    }

    public LocalDate getCreated() { return created; }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostDTO postDTO = (PostDTO) o;
        return Objects.equals(id, postDTO.id) &&
                Objects.equals(created, postDTO.created) &&
                Objects.equals(title, postDTO.title) &&
                Objects.equals(content, postDTO.content) &&
                Objects.equals(categoryId, postDTO.categoryId);
    }
}