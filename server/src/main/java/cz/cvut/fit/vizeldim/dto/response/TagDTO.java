package cz.cvut.fit.vizeldim.dto.response;

import cz.cvut.fit.vizeldim.dto.create.TagCreateDTO;

import java.util.Objects;

public class TagDTO extends TagCreateDTO {
    private final Long id;

    public TagDTO(Long id,
                  String name)
    {
        super(name);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        TagDTO tagDTO = (TagDTO) o;
        return Objects.equals(id, tagDTO.id) &&
                Objects.equals(name, tagDTO.name);
    }
}