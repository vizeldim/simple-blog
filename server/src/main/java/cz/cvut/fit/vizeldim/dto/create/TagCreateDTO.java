package cz.cvut.fit.vizeldim.dto.create;

import java.util.Objects;

public class TagCreateDTO {
    protected final String name;

    public TagCreateDTO(String name) {
        this.name = name;
    }

    public TagCreateDTO() {
        name = null;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TagCreateDTO that = (TagCreateDTO) o;
        return Objects.equals(name, that.name);
    }
}
