package cz.cvut.fit.vizeldim.service;

import cz.cvut.fit.vizeldim.dto.create.CommentCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.CommentDTO;
import cz.cvut.fit.vizeldim.entity.Comment;
import cz.cvut.fit.vizeldim.entity.Post;
import cz.cvut.fit.vizeldim.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CommentService extends MyService<Comment,
                                              CommentDTO,
                                              CommentCreateDTO>
{
    private final CommentRepository commentRepository;

    private final PostService postService;

    @Autowired
    public CommentService(CommentRepository commentRepository, PostService postService) {
        this.commentRepository = commentRepository;
        this.postService = postService;
    }

    @Override
    public CommentRepository getRep() {
        return commentRepository;
    }

    @Override
    public CommentDTO toDTO(Comment entity) {
        return new CommentDTO(
                entity.getId(),
                entity.getContent(),
                entity.getAdded(),
                entity.getToPost().getId());
    }

    @Override
    public Optional<Comment> toEntity(CommentCreateDTO createDTO) {
        if(isValid(createDTO)){
            Optional<Post> optionalPost = postService.findByIdAsEntity(createDTO.getToPostId());
            return optionalPost.map(post -> new Comment(createDTO.getContent(), post));
        }

        return Optional.empty();
    }

    @Override
    public Comment updateEntity(Comment entity, CommentCreateDTO createDTO) {
        if (createDTO.getContent()!=null)
            entity.setContent(createDTO.getContent());

        if (createDTO.getToPostId()!=null){
            Optional<Post> optionalPost = postService.findByIdAsEntity(createDTO.getToPostId());
            optionalPost.ifPresent(entity::setToPost);
        }

        return entity;
    }

    @Override
    public boolean isEmpty(CommentCreateDTO createDTO) {
        return  createDTO.getToPostId()==null &&
                createDTO.getContent()==null;
    }

    @Override
    public boolean isValid(CommentCreateDTO createDTO) {
        return  createDTO.getToPostId()!=null &&
                createDTO.getContent()!=null;
    }

    @Override
    protected Optional<Long> getId(Comment entity) {
        if (entity.getId() == null)
            return Optional.empty();
        return Optional.of(entity.getId());
    }

    public Optional<Page<CommentDTO>> findPageFromPost(Pageable page, Long postId) {
        Optional<Post> optionalPost = postService.findByIdAsEntity(postId);

        return optionalPost.map(post -> getRep()
                .findAllByToPost(page, post)
                .map(this::toDTO));
    }
}
