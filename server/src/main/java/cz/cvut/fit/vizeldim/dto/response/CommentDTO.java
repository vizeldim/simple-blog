package cz.cvut.fit.vizeldim.dto.response;

import cz.cvut.fit.vizeldim.dto.create.CommentCreateDTO;
import java.time.LocalDate;
import java.util.Objects;

public class CommentDTO extends CommentCreateDTO {
    private final Long id;

    private final LocalDate added;

    public CommentDTO(Long id,
                      String content,
                      LocalDate added,
                      Long toPostId)
    {
        super(content,toPostId);
        this.id = id;
        this.added = added;
    }

    public Long getId() {
        return id;
    }

    public LocalDate getAdded() { return added; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CommentDTO that = (CommentDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(added, that.added) &&
                Objects.equals(content, that.content) &&
                Objects.equals(toPostId, that.toPostId);
    }

}