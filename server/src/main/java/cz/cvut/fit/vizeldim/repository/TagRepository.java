package cz.cvut.fit.vizeldim.repository;

import cz.cvut.fit.vizeldim.entity.Post;
import cz.cvut.fit.vizeldim.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TagRepository extends JpaRepository<Tag,Long> {
    List<Tag> findAllByPostsContains(Post post);
}
