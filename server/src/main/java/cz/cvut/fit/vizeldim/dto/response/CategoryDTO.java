package cz.cvut.fit.vizeldim.dto.response;

import cz.cvut.fit.vizeldim.dto.create.CategoryCreateDTO;

import java.util.Objects;

public class CategoryDTO extends CategoryCreateDTO {
    private final Long id;

    public CategoryDTO(Long id,
                       String name,
                       String description)
    {
        super(name, description);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryDTO that = (CategoryDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description);
    }

}