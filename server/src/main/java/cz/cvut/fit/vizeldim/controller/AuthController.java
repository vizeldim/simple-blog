package cz.cvut.fit.vizeldim.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class AuthController {
    @GetMapping("/auth")
    @ResponseStatus(HttpStatus.OK)
    public void authenticated(){ }
}
