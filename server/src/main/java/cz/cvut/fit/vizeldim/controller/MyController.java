package cz.cvut.fit.vizeldim.controller;

import cz.cvut.fit.vizeldim.dto.response.PageDTO;
import cz.cvut.fit.vizeldim.service.MyService;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

//Entity, DTO, CreateDTO
public abstract class MyController<Entity,DTO,CreateDTO> {

    protected abstract MyService<Entity,DTO,CreateDTO> getService();

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public long create(@RequestBody CreateDTO data){
        Optional<Long> optionalId = getService().create(data);
        if (optionalId.isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        return optionalId.get();
    }

    @GetMapping(params = {"page","size"})
    @ResponseStatus(HttpStatus.OK)
    public PageDTO<DTO> readAll(@RequestParam int page,
                                @RequestParam int size)
    {
        return new PageDTO<>(
                getService()
                        .findPage(PageRequest.of(page,size))
        );
    }


    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public DTO readOne(@PathVariable long id){
        Optional<DTO> optionalDTO = getService().findByIdAsDTO(id);

        return optionalDTO.orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable long id,
                       @RequestBody CreateDTO newData)
    {
        if (getService().isEmpty(newData))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        if (!getService().update(id, newData))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id){ getService().deleteById(id); }

}