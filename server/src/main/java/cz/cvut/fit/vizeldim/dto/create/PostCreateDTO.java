package cz.cvut.fit.vizeldim.dto.create;

import java.util.Objects;

public class PostCreateDTO {
    protected final String title;

    protected final String content;

    protected final Long categoryId;

    public PostCreateDTO(String title, String content, Long categoryId) {
        this.title = title;
        this.content = content;
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostCreateDTO that = (PostCreateDTO) o;
        return Objects.equals(title, that.title) &&
                Objects.equals(content, that.content) &&
                Objects.equals(categoryId, that.categoryId);
    }
}
