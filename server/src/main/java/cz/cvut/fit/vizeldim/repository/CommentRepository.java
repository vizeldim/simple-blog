package cz.cvut.fit.vizeldim.repository;

import cz.cvut.fit.vizeldim.entity.Comment;
import cz.cvut.fit.vizeldim.entity.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment,Long> {
    Page<Comment> findAllByToPost(Pageable page, Post post);
}
