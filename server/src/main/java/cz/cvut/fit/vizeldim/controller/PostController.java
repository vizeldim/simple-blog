package cz.cvut.fit.vizeldim.controller;

import cz.cvut.fit.vizeldim.dto.create.IdList;
import cz.cvut.fit.vizeldim.dto.create.PostCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.PageDTO;
import cz.cvut.fit.vizeldim.dto.response.PostDTO;
import cz.cvut.fit.vizeldim.entity.Post;
import cz.cvut.fit.vizeldim.entity.Tag;
import cz.cvut.fit.vizeldim.service.PostService;
import cz.cvut.fit.vizeldim.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

@RestController
@RequestMapping("/posts")
public class PostController  extends MyController<Post, PostDTO, PostCreateDTO>{
    private final PostService postService;

    private final TagService tagService;

    @Autowired
    public PostController(PostService postService, TagService tagService) {
        this.postService = postService;
        this.tagService = tagService;
    }

    @Override
    protected PostService getService() { return postService; }

    @GetMapping(params = {"page", "size", "category"})
    @ResponseStatus(HttpStatus.OK)
    public PageDTO<PostDTO> readPageInCategory(@RequestParam int page,
                                            @RequestParam int size,
                                            @RequestParam long category)
    {
        return new PageDTO<>(
            getService()
                .findPageFromCategory(PageRequest.of(page,size), category)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND))
        );
    }

    @GetMapping(params = {"page", "size", "tag"})
    @ResponseStatus(HttpStatus.OK)
    public PageDTO<PostDTO> readPageWithTag(@RequestParam int page,
                                             @RequestParam int size,
                                             @RequestParam long tag)
    {
        Optional<Tag> optionalTag = tagService.findByIdAsEntity(tag);

        if (optionalTag.isPresent())
        {
            return new PageDTO<>(
                    getService()
                        .findPageWithTag(PageRequest.of(page, size),
                                         optionalTag.get())
            );
        }
        else
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}/addTags")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void addTags(@PathVariable long id,
                        @RequestBody IdList ids)
    {
        Set<Tag> tagList = tagService
                .findByIdsAsEntities(new HashSet<>(ids.ids));

        if (tagList.isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        getService().addTags(readPostEntity(id), tagList);
    }

    @PutMapping("/{id}/removeTags")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeTags( @PathVariable long id,
                            @RequestBody IdList ids )
    {
        Set<Tag> tagList = tagService
                .findByIdsAsEntities(new HashSet<>(ids.ids));

        if (tagList.isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        getService().removeTags(readPostEntity(id), tagList);
    }

    /*------------Helpers-----------------*/
    public Post readPostEntity(long id){
        Optional<Post> optionalPost = getService().findByIdAsEntity(id);
        if (optionalPost.isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        return optionalPost.get();
    }
}