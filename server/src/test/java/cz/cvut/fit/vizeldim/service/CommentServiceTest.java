package cz.cvut.fit.vizeldim.service;

import cz.cvut.fit.vizeldim.dto.create.CommentCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.CommentDTO;
import cz.cvut.fit.vizeldim.entity.Category;
import cz.cvut.fit.vizeldim.entity.Comment;
import cz.cvut.fit.vizeldim.entity.Post;
import cz.cvut.fit.vizeldim.repository.CommentRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

@SpringBootTest
public class CommentServiceTest {
    @MockBean
    private CommentRepository repository;

    @MockBean
    private PostService postService;

    @InjectMocks
    @Autowired
    private CommentService service;

    private static final String testContent = "name";
    private static final String testContentOther = "name2";

    private static final Long testPostId = (long)13;
    private static final Post testPost = new Post("title",
                                                "content",
                                                        new Category("name",
                                                                "description"));


    private static final Comment testEntity = new Comment(testContent, testPost);
    private static final CommentDTO testDTO = new CommentDTO(testEntity.getId(),
                                                             testContent,
                                                             testEntity.getAdded(),
                                                             testPost.getId());
    private static final CommentCreateDTO testCreateDTO = new CommentCreateDTO(testContent,testPostId);

    private static final Comment testEntityOther = new Comment(testContentOther,testPost);
    private static final CommentCreateDTO testCreateDTOOther = new CommentCreateDTO(testContentOther,testPostId);

    private static final CommentCreateDTO testCreateDTOInvalid = new CommentCreateDTO(null,testPostId);
    private static final CommentCreateDTO testCreateDTOEmpty = new CommentCreateDTO(null,null);


    @Test
    public void testGetRep(){ assertThat(service.getRep()).isEqualTo(repository); }

    @Test
    public void testIsValid(){
        assertTrue(service.isValid(testCreateDTO));
        assertFalse(service.isValid(testCreateDTOInvalid));
    }

    @Test
    public void testIsEmpty(){
        assertFalse(service.isEmpty(testCreateDTO));
        assertTrue(service.isEmpty(testCreateDTOEmpty));
    }

    @Test
    public void testToDTO(){
        CommentDTO dto = service.toDTO(testEntity);
        assertThat(dto).isEqualTo(testDTO);
    }

    @Test
    public void testToEntity(){
        Mockito
                .when(postService.findByIdAsEntity(testCreateDTO.getToPostId()))
                .thenReturn(Optional.of(testPost));

        Optional<Comment> comment = service.toEntity(testCreateDTO);

        assertThat(comment)
                .isEqualTo(Optional.of(testEntity));
    }

    @Test
    public void testToEntityInvalid(){
        assertThat(service.toEntity(testCreateDTOInvalid))
                .isEqualTo(Optional.empty());
    }

    @Test
    public void testUpdateEntity(){
        assertThat(service.updateEntity(testEntity,
                testCreateDTOOther))
                .isEqualTo(testEntityOther);

        //ROLLBACK
        testEntity.setToPost(testPost);
        testEntity.setContent(testContent);
    }

    @Test
    public void testCreateInvalid(){
        assertEquals(Optional.empty(),service.create(testCreateDTOInvalid));
    }

    @Test
    public void testFindByIdAsDTO(){
        long id = 13; //Some random id for Mock

        Mockito
                .when(service.getRep().findById(id))
                .thenReturn(Optional.of(testEntity));

        assertThat(service.findByIdAsDTO(id)).
                isEqualTo(Optional.of(testDTO));
    }

    @Test
    public void testFindByIdNotExist(){
        long id = 13; //Some random id

        Mockito
                .when(service.getRep().findById(id))
                .thenReturn(Optional.empty());

        Optional<CommentDTO> optionalFoundCommentDTO = service.findByIdAsDTO(id);

        assertThat(optionalFoundCommentDTO)
                .isEqualTo(Optional.empty());
    }

    @Test
    public void testFindByIdAsEntity(){
        long id = 13; //Some random id

        Mockito
                .when(service.getRep().findById(id))
                .thenReturn(Optional.of(testEntity));

        assertThat(service.findByIdAsEntity(id))
                .isEqualTo(Optional.of(testEntity));
    }

    @Test
    public void testFindByIdAsEntityNotExist(){
        long id = 13; //Some random id

        Mockito
                .when(service.getRep().findById(id))
                .thenReturn(Optional.empty());

        assertThat(service.findByIdAsEntity(id))
                .isEqualTo(Optional.empty());
    }

    @Test
    public void testUpdate(){
        long id = 13; //Some random id for mock

        Mockito.when(service.getRep().findById(id))
                .thenReturn(Optional.of(testEntity));

        assertTrue(service.update(id, testCreateDTO));
    }

    @Test
    public void testUpdateNotInDB(){
        long id = 13; //Some random id for mock

        Mockito
                .when(service.getRep().findById(id))
                .thenReturn(Optional.empty());

        assertFalse(service.update(id, testCreateDTO));
    }

    @Test
    public void testFindPage(){
        List<Comment> commentList = new ArrayList<>();
        commentList.add(testEntity);
        commentList.add(testEntityOther);

        Pageable page = PageRequest.of(0,10);
        Page<Comment> commentPage = new PageImpl<>(commentList);

        Mockito
                .when(service.getRep().findAll(page))
                .thenReturn(commentPage);

        assertThat(service.findPage(page))
                .isEqualTo(commentPage.map(service::toDTO));
    }


    /*------------------ONLY COMMENT----------------------------*/
    @Test
    public void testFindPageFromPost(){
        List<Comment> commentList = new ArrayList<>();
        commentList.add(testEntity);
        commentList.add(testEntityOther);

        Pageable page = PageRequest.of(0,10);
        Page<Comment> commentPage = new PageImpl<>(commentList);

        Mockito
                .when(postService.findByIdAsEntity(testPost.getId()))
                .thenReturn(Optional.of(testPost));

        Mockito
                .when(service.getRep().findAllByToPost(page,testPost))
                .thenReturn(commentPage);

        assertThat(service.findPageFromPost(page,testPost.getId()))
                .isEqualTo(Optional.of(commentPage.map(service::toDTO)));
    }

    @Test
    public void testFindPageFromPostNotExist(){
        Pageable page = PageRequest.of(0,10);

        Mockito
                .when(postService.findByIdAsEntity(testPostId))
                .thenReturn(Optional.empty());

        assertThat(service.findPageFromPost(page,testPost.getId()))
                .isEqualTo(Optional.empty());
    }

    @Test
    public void testGetIdNull(){
        assertEquals(Optional.empty(),service.getId(testEntity));
    }
}
