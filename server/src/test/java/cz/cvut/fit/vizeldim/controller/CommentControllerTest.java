package cz.cvut.fit.vizeldim.controller;

import cz.cvut.fit.vizeldim.dto.create.CommentCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.CommentDTO;
import cz.cvut.fit.vizeldim.dto.response.PageDTO;
import cz.cvut.fit.vizeldim.entity.Comment;
import cz.cvut.fit.vizeldim.service.CommentService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CommentControllerTest extends MyControllerTest<Comment, CommentDTO, CommentCreateDTO>{
    @MockBean
    private CommentService service;

    private static final CommentDTO commentDTO = new CommentDTO(id, "TestContent", LocalDate.now(), id);
    private static final CommentCreateDTO commentCreateDTO = new CommentCreateDTO("TestContent",id);

    @Override
    protected CommentService getService() { return service; }

    @Override
    protected CommentDTO getDTO_Test() { return commentDTO; }

    @Override
    protected CommentCreateDTO getCreateDTO_Test() { return commentCreateDTO; }

    @Override
    protected String getUrl() { return "comments"; }

    @Test
    public void readPageInPost() throws Exception {
        List<CommentDTO> commentDTOList = new ArrayList<>();
        commentDTOList.add(commentDTO);

        Pageable page = PageRequest.of(0,10);
        Page<CommentDTO> commentPage = new PageImpl<>(commentDTOList);

        PageDTO<CommentDTO> commentDTOPage = new PageDTO<>(commentPage);

        Mockito.when(service
                        .findPageFromPost(page,(long)13))
                        .thenReturn(Optional.of(commentPage));

        MvcResult result = mockMvc
                .perform(get("/comments")
                         .param("page", "0")
                         .param("size","10")
                         .param("post","13"))
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();

        String expectedResponseBody =
                objectMapper.writeValueAsString(commentDTOPage);

        assertThat(response)
                .isEqualToIgnoringWhitespace(expectedResponseBody);
    }

    //Does not require authorization
    @Override
    public void testCreateUnauthorized() throws Exception {}

    @Override
    public void testUpdateUnauthorized() throws Exception {}
}