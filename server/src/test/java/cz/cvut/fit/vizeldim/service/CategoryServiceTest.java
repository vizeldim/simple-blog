package cz.cvut.fit.vizeldim.service;

import cz.cvut.fit.vizeldim.dto.create.CategoryCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.CategoryDTO;
import cz.cvut.fit.vizeldim.entity.Category;
import cz.cvut.fit.vizeldim.repository.CategoryRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

@SpringBootTest
public class CategoryServiceTest{
    @MockBean
    private CategoryRepository repository;

    @InjectMocks
    @Autowired
    private CategoryService service;

    private static final String testName = "name";
    private static final String testDescription = "description";
    private static final String testNameOther = "name2";
    private static final String testDescriptionOther = "description2";

    private static final Category testEntity = new Category(testName, testDescription);
    private static final CategoryDTO testDTO = new CategoryDTO(testEntity.getId(),testName,testDescription);
    private static final CategoryCreateDTO testCreateDTO = new CategoryCreateDTO(testName,testDescription);

    private static final Category testEntityOther = new Category(testNameOther,testDescriptionOther);
    private static final CategoryCreateDTO testCreateDTOOther = new CategoryCreateDTO(testNameOther,testDescriptionOther);

    private static final CategoryCreateDTO testCreateDTOInvalid = new CategoryCreateDTO(null,testDescription);
    private static final CategoryCreateDTO testCreateDTOEmpty = new CategoryCreateDTO(null,null);


    @Test
    public void testGetRep(){ assertThat(service.getRep()).isEqualTo(repository); }

    @Test
    public void testIsValid(){
        assertTrue(service.isValid(testCreateDTO));
        assertFalse(service.isValid(testCreateDTOInvalid));
    }

    @Test
    public void testIsEmpty(){
        assertFalse(service.isEmpty(testCreateDTO));
        assertTrue(service.isEmpty(testCreateDTOEmpty));
    }

    @Test
    public void testToDTO(){
        CategoryDTO dto = service.toDTO(testEntity);
        assertThat(dto).isEqualTo(testDTO);
    }

    @Test
    public void testToEntity(){
        assertThat(service.toEntity(testCreateDTO))
                .isEqualTo(Optional.of(testEntity));
    }

    @Test
    public void testToEntityInvalid(){
        assertThat(service.toEntity(testCreateDTOInvalid))
                .isEqualTo(Optional.empty());
    }

    @Test
    public void testUpdateEntity(){
        assertThat(service.updateEntity(testEntity,
                testCreateDTOOther))
                .isEqualTo(testEntityOther);

        //ROLLBACK
        testEntity.setName(testName);
        testEntity.setDescription(testDescription);
    }

    @Test
    public void testCreateInvalid(){
        assertEquals(Optional.empty(), service.create(testCreateDTOInvalid));
    }

    @Test
    public void testFindByIdAsDTO(){
        long id = 13; //Some random id for Mock

        Mockito
            .when(service.getRep().findById(id))
            .thenReturn(Optional.of(testEntity));

        assertThat(service.findByIdAsDTO(id)).
                isEqualTo(Optional.of(testDTO));
    }

    @Test
    public void testFindByIdNotExist(){
        long id = 13; //Some random id

        Mockito
            .when(service.getRep().findById(id))
            .thenReturn(Optional.empty());

        Optional<CategoryDTO> optionalFoundCategoryDTO = service.findByIdAsDTO(id);

        assertThat(optionalFoundCategoryDTO)
                .isEqualTo(Optional.empty());
    }

    @Test
    public void testFindByIdAsEntity(){
        long id = 13; //Some random id

        Mockito
            .when(service.getRep().findById(id))
            .thenReturn(Optional.of(testEntity));

        assertThat(service.findByIdAsEntity(id))
            .isEqualTo(Optional.of(testEntity));
    }

    @Test
    public void testFindByIdAsEntityNotExist(){
        long id = 13; //Some random id

        Mockito
                .when(service.getRep().findById(id))
                .thenReturn(Optional.empty());

        assertThat(service.findByIdAsEntity(id))
                .isEqualTo(Optional.empty());
    }

    @Test
    public void testUpdate(){
        long id = 13; //Some random id for mock

        Mockito.when(service.getRep().findById(id))
                .thenReturn(Optional.of(testEntity));

        assertTrue(service.update(id, testCreateDTO));
    }

    @Test
    public void testUpdateNotInDB(){
        long id = 13; //Some random id for mock

        Mockito
            .when(service.getRep().findById(id))
            .thenReturn(Optional.empty());

        assertFalse(service.update(id, testCreateDTO));
    }

    @Test
    public void testFindPage(){
        List<Category> categoryList = new ArrayList<>();
        categoryList.add(testEntity);
        categoryList.add(testEntityOther);

        Pageable page = PageRequest.of(0,10);
        Page<Category> categoryPage = new PageImpl<>(categoryList);

        Mockito
                .when(service.getRep().findAll(page))
                .thenReturn(categoryPage);

        assertThat(service.findPage(page))
                .isEqualTo(categoryPage.map(service::toDTO));
    }

    @Test
    public void testFindAll(){
        List<Category> categoryList = new ArrayList<>();
        categoryList.add(testEntity);
        categoryList.add(testEntityOther);

        Mockito
                .when(repository.findAll())
                .thenReturn(categoryList);

        assertThat(service.findAll())
                .isEqualTo(
                        categoryList
                        .stream()
                        .map(service::toDTO)
                        .collect(Collectors.toList()));
    }

    @Test
    public void testGetIdNull(){
        assertEquals(Optional.empty(),service.getId(testEntity));
    }
}
