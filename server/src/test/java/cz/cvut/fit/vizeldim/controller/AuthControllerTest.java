package cz.cvut.fit.vizeldim.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class AuthControllerTest {
    @Autowired
    protected MockMvc mockMvc;

    @Test
    public void testAuthenticatedNOT() throws Exception {
        mockMvc
                .perform(get("/auth"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "admin", password = "password", roles = "USER")
    public void testAuthenticatedIS() throws Exception {
        mockMvc
                .perform(get("/auth"))
                .andExpect(status().isOk());
    }
}
