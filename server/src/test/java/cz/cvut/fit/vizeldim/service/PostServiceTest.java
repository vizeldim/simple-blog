package cz.cvut.fit.vizeldim.service;

import cz.cvut.fit.vizeldim.dto.create.PostCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.PostDTO;
import cz.cvut.fit.vizeldim.entity.Category;
import cz.cvut.fit.vizeldim.entity.Post;
import cz.cvut.fit.vizeldim.entity.Tag;
import cz.cvut.fit.vizeldim.repository.PostRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

@SpringBootTest
public class PostServiceTest {
    @MockBean
    private PostRepository repository;

    @MockBean
    private CategoryService categoryService;

    @InjectMocks
    @Autowired
    private PostService service;

    private static final String testName = "name";
    private static final String testContent = "content";
    private static final String testNameOther = "name2";
    private static final String testContentOther = "content2";

    private static final Category testCategory = new Category("name","description");
    private static final Tag testTag = new Tag("tagName", Collections.emptyList());


    private static final Post testEntity = new Post(testName,testContent,testCategory);
    private static final PostDTO testDTO = new PostDTO( testEntity.getId(),
                                                        testName,
                                                        testContent,
                                                        testEntity.getCreated(),
                                                        testCategory.getId() );
    private static final PostCreateDTO testCreateDTO = new PostCreateDTO(testName,testContent,(long)13);

    private static final Post testEntityOther = new Post(testNameOther,testContentOther, testCategory);
    private static final PostCreateDTO testCreateDTOOther = new PostCreateDTO(  testNameOther,
                                                                                testContentOther,
                                                                                testCategory.getId() );

    private static final PostCreateDTO testCreateDTOInvalid = new PostCreateDTO(null,testContentOther,testCategory.getId());
    private static final PostCreateDTO testCreateDTOEmpty = new PostCreateDTO(null,null, null);


    @Test
    public void testGetRep(){ assertThat(service.getRep()).isEqualTo(repository); }

    @Test
    public void testIsValid(){
        assertTrue(service.isValid(testCreateDTO));
        assertFalse(service.isValid(testCreateDTOInvalid));
    }

    @Test
    public void testIsEmpty(){
        assertFalse(service.isEmpty(testCreateDTO));
        assertTrue(service.isEmpty(testCreateDTOEmpty));
    }

    @Test
    public void testToDTO(){
        PostDTO dto = service.toDTO(testEntity);
        assertThat(dto).isEqualTo(testDTO);
    }

    @Test
    public void testToEntity(){
        Mockito
                .when(categoryService.findByIdAsEntity(testCreateDTO.getCategoryId()))
                .thenReturn(Optional.of(testCategory));

        assertThat(service.toEntity(testCreateDTO))
                .isEqualTo(Optional.of(testEntity));
    }

    @Test
    public void testToEntityInvalid(){
        assertThat(service.toEntity(testCreateDTOInvalid))
                .isEqualTo(Optional.empty());
    }

    @Test
    public void testUpdateEntity(){
        assertThat(service.updateEntity(testEntity,
                testCreateDTOOther))
                .isEqualTo(testEntityOther);

        //ROLLBACK
        testEntity.setTitle(testName);
        testEntity.setContent(testContent);
        testEntity.setCategory(testCategory);
    }

    @Test
    public void testCreateInvalid(){
        assertEquals(Optional.empty(), service.create(testCreateDTOInvalid));
    }

    @Test
    public void testFindByIdAsDTO(){
        long id = 13; //Some random id for Mock

        Mockito
                .when(service.getRep().findById(id))
                .thenReturn(Optional.of(testEntity));

        assertThat(service.findByIdAsDTO(id)).
                isEqualTo(Optional.of(testDTO));
    }

    @Test
    public void testFindByIdNotExist(){
        long id = 13; //Some random id

        Mockito
                .when(service.getRep().findById(id))
                .thenReturn(Optional.empty());

        Optional<PostDTO> optionalFoundPostDTO = service.findByIdAsDTO(id);

        assertThat(optionalFoundPostDTO)
                .isEqualTo(Optional.empty());
    }

    @Test
    public void testFindByIdAsEntity(){
        long id = 13; //Some random id

        Mockito
                .when(service.getRep().findById(id))
                .thenReturn(Optional.of(testEntity));

        assertThat(service.findByIdAsEntity(id))
                .isEqualTo(Optional.of(testEntity));
    }

    @Test
    public void testFindByIdAsEntityNotExist(){
        long id = 13; //Some random id

        Mockito
                .when(service.getRep().findById(id))
                .thenReturn(Optional.empty());

        assertThat(service.findByIdAsEntity(id))
                .isEqualTo(Optional.empty());
    }

    @Test
    public void testUpdate(){
        long id = 13; //Some random id for mock

        Mockito.when(service.getRep().findById(id))
                .thenReturn(Optional.of(testEntity));

        assertTrue(service.update(id, testCreateDTO));
    }

    @Test
    public void testUpdateNotInDB(){
        long id = 13; //Some random id for mock

        Mockito
                .when(service.getRep().findById(id))
                .thenReturn(Optional.empty());

        assertFalse(service.update(id, testCreateDTO));
    }

    @Test
    public void testFindPage(){
        List<Post> postList = new ArrayList<>();
        postList.add(testEntity);
        postList.add(testEntityOther);

        Pageable page = PageRequest.of(0,10);
        Page<Post> postPage = new PageImpl<>(postList);

        Mockito
                .when(service.getRep().findAll(page))
                .thenReturn(postPage);

        assertThat(service.findPage(page))
                .isEqualTo(postPage.map(service::toDTO));
    }

    /*------------------ONLY POST--------------------------*/
    @Test
    public void testFindPageFromCategory(){
        List<Post> postList = new ArrayList<>();
        postList.add(testEntity);
        postList.add(testEntityOther);

        Pageable page = PageRequest.of(0,10);
        Page<Post> postPage = new PageImpl<>(postList);

        Mockito
                .when(categoryService.findByIdAsEntity((long)13))
                .thenReturn(Optional.of(testCategory));

        Mockito
                .when(repository.findAllByCategory(page,testCategory))
                .thenReturn(postPage);

        assertThat(service.findPageFromCategory(page,13))
                .isEqualTo(Optional.of(postPage.map(service::toDTO)));
    }

    @Test
    public void testFindPageFromCategoryNotExist(){
        Pageable page = PageRequest.of(0,10);

        Mockito
                .when(categoryService.findByIdAsEntity((long)13))
                .thenReturn(Optional.empty());

        assertThat(service.findPageFromCategory(page,13))
                .isEqualTo(Optional.empty());
    }


    @Test
    public void testFindPageWithTag(){
        List<Post> postList = new ArrayList<>();
        postList.add(testEntity);
        postList.add(testEntityOther);

        Pageable page = PageRequest.of(0,10);
        Page<Post> postPage = new PageImpl<>(postList);

        Mockito
                .when(repository.findAllByTagsContains(page,testTag))
                .thenReturn(postPage);

        assertThat(service.findPageWithTag(page,testTag))
                .isEqualTo(postPage.map(service::toDTO));
    }

    @Test
    public void testGetIdNull(){
        assertEquals(Optional.empty(),service.getId(testEntity));
    }
}