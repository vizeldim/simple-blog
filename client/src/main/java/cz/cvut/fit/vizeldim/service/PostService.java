package cz.cvut.fit.vizeldim.service;

import cz.cvut.fit.vizeldim.dto.create.PostCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.PostDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostService {
    public List<PostDTO> shortenPostList(List<PostDTO> dtoList, int contentLength){
        return dtoList
                .stream()
                .peek((post)-> {
                    if(post.content.length()>contentLength)
                        post.content = post.content.substring(0,contentLength)+"...";
                })
                .collect(Collectors.toList());
    }

    public boolean validate(PostCreateDTO postCreateDTO) {
        return postCreateDTO.title.length() > 0 && postCreateDTO.content.length() > 0;
    }
}
