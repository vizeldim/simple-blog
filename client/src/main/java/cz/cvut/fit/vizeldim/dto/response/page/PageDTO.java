package cz.cvut.fit.vizeldim.dto.response.page;

import java.util.List;

public abstract class PageDTO<DTO> {
    public List<DTO> dtoList;
    public int totalPages;
}
