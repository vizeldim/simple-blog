package cz.cvut.fit.vizeldim;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
    private final RestTemplate restTemplate;
    private final String AUTH_URL = "/auth";

    @Autowired
    public CustomAuthenticationProvider(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
            String username = authentication.getName();
            String password = authentication.getCredentials().toString();

            if (authenticateAPI(username,password)) {
                return new UsernamePasswordAuthenticationToken
                        (username, password, Collections.emptyList());
            }
            else
                throw new BadCredentialsException("Authentication failed");
    }

    private boolean authenticateAPI(String username, String password) {
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(username, password);

        HttpEntity request = new HttpEntity(headers);
        HttpStatus status = null;
        try {
           status = restTemplate.exchange(AUTH_URL, HttpMethod.GET, request, void.class).getStatusCode();
        }
        catch(RestClientException e){return false;}

        return status == HttpStatus.OK;
    }

    @Override
    public boolean supports(Class<?> auth) {
        return auth.equals(UsernamePasswordAuthenticationToken.class);
    }


}
