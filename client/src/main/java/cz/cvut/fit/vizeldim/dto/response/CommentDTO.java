package cz.cvut.fit.vizeldim.dto.response;

import cz.cvut.fit.vizeldim.dto.create.CommentCreateDTO;

public class CommentDTO extends CommentCreateDTO {
    public Long id;
    public String added;
}
