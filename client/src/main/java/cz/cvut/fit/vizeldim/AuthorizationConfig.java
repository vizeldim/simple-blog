package cz.cvut.fit.vizeldim;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
@EnableWebSecurity
public class AuthorizationConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/admin/**").authenticated()
                .antMatchers("/posts/*/edit").authenticated()
                .antMatchers("/posts/*/delete").authenticated()
                .antMatchers("/posts/*/delete").authenticated()
                .antMatchers("/posts/new").authenticated()
                .antMatchers("/posts/add").authenticated()
                .antMatchers("/posts/*/update").authenticated()
                .antMatchers("/categories/new").authenticated()
                .antMatchers("/categories/add").authenticated()
                .antMatchers("/tags/new").authenticated()
                .antMatchers("/tags/add").authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll();
    }

}