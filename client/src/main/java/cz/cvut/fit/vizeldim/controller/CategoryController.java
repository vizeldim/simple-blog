package cz.cvut.fit.vizeldim.controller;

import cz.cvut.fit.vizeldim.client.CategoryClient;
import cz.cvut.fit.vizeldim.client.PostClient;
import cz.cvut.fit.vizeldim.client.TagClient;
import cz.cvut.fit.vizeldim.dto.create.CategoryCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.CategoryDTO;
import cz.cvut.fit.vizeldim.dto.response.page.PostPageDTO;
import cz.cvut.fit.vizeldim.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/categories")
public class CategoryController {
    private final CategoryClient categoryClient;
    private final TagClient tagClient;
    private final PostClient postClient;
    private final PostService postService;

    @Autowired
    public CategoryController(CategoryClient categoryClient, TagClient tagClient, PostClient postClient, PostService postService) {
        this.categoryClient = categoryClient;
        this.tagClient = tagClient;
        this.postClient = postClient;
        this.postService = postService;
    }

    @RequestMapping
    public String categories(Model model){
        model.addAttribute("title","Categories & Tags");
        model.addAttribute("categories", categoryClient.getAll());
        model.addAttribute("tags",tagClient.getAll());
        return "categories";
    }

    @RequestMapping("/{id}")
    public String category(Model model, @PathVariable int id, @RequestParam(required = false) Integer page){
        CategoryDTO categoryDTO = categoryClient.getItem(id);

        if (page == null)
            page = 0;
        else
            --page;

        PostPageDTO postDTOList = postClient
                .getPageFromCategory(categoryDTO.id, page,Constants.POSTS_PER_PAGE, Constants.POST_INTRO_LENGTH);

        model.addAttribute("title", categoryDTO.name);
        model.addAttribute("uid", categoryDTO.id);

        model.addAttribute("posts", postDTOList.dtoList);
        model.addAttribute("nextPageUrl", "categories");

        if (postDTOList.totalPages == 0)
            postDTOList.totalPages = 1;

        model.addAttribute("pages", postDTOList.totalPages);
        model.addAttribute("current_page", page+1);

        return "category";
    }

    @RequestMapping("/all")
    public String all(Model model,@RequestParam(required = false) Integer page){
        if (page==null){page=0;}else {--page;}

        PostPageDTO postDTOList = postClient
                .getPage(page,Constants.POSTS_PER_PAGE);

        postDTOList.dtoList = postService.shortenPostList(postDTOList.dtoList, Constants.POST_INTRO_LENGTH);

        model.addAttribute("title","All Posts");
        model.addAttribute("uid", "all");

        model.addAttribute("posts", postDTOList.dtoList);
        model.addAttribute("nextPageUrl", "categories");

        if (postDTOList.totalPages == 0)
            postDTOList.totalPages = 1;

        model.addAttribute("pages", postDTOList.totalPages);
        model.addAttribute("current_page", page+1);
        return "category";
    }

    @RequestMapping("/new")
    public String newCategory(Model model){
        model.addAttribute("title","New Category");

        return "newcategory";
    }

    @RequestMapping("/add")
    public String addCategory(@RequestParam String name, @RequestParam String description){
        CategoryCreateDTO categoryCreateDTO = new CategoryCreateDTO();
        categoryCreateDTO.name = name;
        categoryCreateDTO.description = description;

        if (name.length() > 0)
        categoryClient.addCategory(categoryCreateDTO);

        return "redirect:/admin";
    }
}
