package cz.cvut.fit.vizeldim.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AdminController {

    @RequestMapping("/admin")
    public String admin(Model model){
        model.addAttribute("title", "Admin Panel");
        return "admin";
    }

    @RequestMapping("/login")
    public String login(Model model){
        model.addAttribute("title", "Log In");
        return "login";
    }
}
