package cz.cvut.fit.vizeldim.dto.response;

import java.io.Serializable;

public class PostDTO implements Serializable {
    public Long id;
    public String created;
    public String title;
    public String content;
    public Long categoryId;
}
