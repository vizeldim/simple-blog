package cz.cvut.fit.vizeldim.controller;

public class Constants {
    public static final int POSTS_PER_PAGE = 7;
    public static final int POST_INTRO_LENGTH = 250;
}
