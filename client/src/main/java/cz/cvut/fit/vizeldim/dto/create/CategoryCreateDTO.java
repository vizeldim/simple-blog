package cz.cvut.fit.vizeldim.dto.create;

public class CategoryCreateDTO {
    public String name;
    public String description;
}
