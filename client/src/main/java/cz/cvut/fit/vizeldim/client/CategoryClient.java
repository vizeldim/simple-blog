package cz.cvut.fit.vizeldim.client;

import cz.cvut.fit.vizeldim.dto.create.CategoryCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.CategoryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CategoryClient {
    private final RestTemplate restTemplate;
    private static final String URL = "/categories";
    private static final String ONE_URI = "/{id}";

    @Autowired
    public CategoryClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public CategoryDTO getItem(long id){
        return restTemplate
                .getForObject(URL + ONE_URI,  CategoryDTO.class, id);
    }

    public CategoryDTO[] getAll(){
        return restTemplate
                .getForObject(URL,CategoryDTO[].class);
    }

    public void addCategory(CategoryCreateDTO category) {
        restTemplate.postForObject(URL,category,Long.class);
    }
}