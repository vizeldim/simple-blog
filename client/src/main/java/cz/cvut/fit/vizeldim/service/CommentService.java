package cz.cvut.fit.vizeldim.service;

import cz.cvut.fit.vizeldim.dto.create.CommentCreateDTO;
import org.springframework.stereotype.Service;

@Service
public class CommentService {
    public boolean validate(CommentCreateDTO commentCreateDTO){
        return commentCreateDTO.content.length() >= 10;
    }
}
