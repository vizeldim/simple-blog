package cz.cvut.fit.vizeldim.dto.create;

public class CommentCreateDTO {
    public String content;
    public Long toPostId;
}
