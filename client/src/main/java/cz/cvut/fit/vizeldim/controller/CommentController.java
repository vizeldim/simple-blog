package cz.cvut.fit.vizeldim.controller;

import cz.cvut.fit.vizeldim.client.CommentClient;
import cz.cvut.fit.vizeldim.dto.create.CommentCreateDTO;
import cz.cvut.fit.vizeldim.service.CommentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/comments")
public class CommentController {
    private final CommentClient commentClient;
    private final CommentService commentService;

    public CommentController(CommentClient commentClient, CommentService commentService) {
        this.commentClient = commentClient;
        this.commentService = commentService;
    }

    @PostMapping
    public String addComment(@RequestParam long post, @RequestParam String content){
        CommentCreateDTO comment = new CommentCreateDTO();
        comment.toPostId = post;
        comment.content = content;

        if (commentService.validate(comment))
            commentClient.addComment(comment);

        return "redirect:/posts/"+post;
    }
}
