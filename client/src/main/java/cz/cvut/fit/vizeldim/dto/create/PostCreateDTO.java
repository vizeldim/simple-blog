package cz.cvut.fit.vizeldim.dto.create;

public class PostCreateDTO {
    public String title;
    public String content;
    public Long categoryId;
}
