package cz.cvut.fit.vizeldim.client;

import cz.cvut.fit.vizeldim.dto.create.TagCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.TagDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class TagClient {
    private final RestTemplate restTemplate;
    private static final String URL = "/tags";
    private static final String ONE_URI = "/{id}";
    private static final String COLLECTION_POST_URI = "/?post={postId}";

    @Autowired
    public TagClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public TagDTO getItem(long id){
        return restTemplate
                .getForObject(URL + ONE_URI,  TagDTO.class, id);
    }

    public TagDTO[] getItems(long postId){
        ResponseEntity<TagDTO[]> response = restTemplate.exchange(
                        URL + COLLECTION_POST_URI,
                        HttpMethod.GET,
                        null,
                        TagDTO[].class,
                        postId);

        return response.getBody();
    }

    public TagDTO[] getAll(){
        return restTemplate
                .getForObject(URL, TagDTO[].class);
    }

    public void addTag(TagCreateDTO tag) {
        restTemplate.postForObject(URL,tag,Long.class);
    }
}