package cz.cvut.fit.vizeldim.client;

import cz.cvut.fit.vizeldim.dto.create.IdList;
import cz.cvut.fit.vizeldim.dto.create.PostCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.PostDTO;
import cz.cvut.fit.vizeldim.dto.response.page.PostPageDTO;
import cz.cvut.fit.vizeldim.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class PostClient {
    private final PostService postService;
    private final RestTemplate restTemplate;
    private static final String URL = "/posts";
    private static final String ONE_URI = "/{id}";
    private static final String COLLECTION_PAGED_URI = "/?page={page}&size={size}";
    private static final String COLLECTION_CATEGORY_PAGED_URI = "/?category={categoryId}&page={page}&size={size}";
    private static final String COLLECTION_TAG_PAGED_URI = "/?tag={tagId}&page={page}&size={size}";

    private static final String URL_ADD_TAGS = "/addTags";
    private static final String URL_REMOVE_TAGS = "/removeTags";


    @Autowired
    public PostClient(PostService postService, RestTemplate restTemplate) {
        this.postService = postService;
        this.restTemplate = restTemplate;
    }

    public PostDTO getItem(long id){
        return restTemplate
                    .getForObject(URL + ONE_URI,  PostDTO.class, id);
    }

    public PostPageDTO getPage(int page, int size){
        return restTemplate.getForObject(
                URL + COLLECTION_PAGED_URI,
                PostPageDTO.class,
                page,
                size);
    }

    public PostPageDTO getPage(int page, int size, int contentLength){
        PostPageDTO postPageDTO = getPage(page, size);

        postPageDTO.dtoList = postService.shortenPostList(postPageDTO.dtoList, contentLength);

        return postPageDTO;
    }

    public PostPageDTO getPageFromCategory(Long categoryId, int page, int size){
        return restTemplate.getForObject(
                URL + COLLECTION_CATEGORY_PAGED_URI,
                PostPageDTO.class,
                categoryId,
                page,
                size);
    }

    public PostPageDTO getPageWithTag(Long tagId, int page, int size){
        return restTemplate.getForObject(
                URL + COLLECTION_TAG_PAGED_URI,
                PostPageDTO.class,
                tagId,
                page,
                size);
    }

    public PostPageDTO getPageFromCategory(Long categoryId, int page, int size, int contentLength){
        PostPageDTO postPageDTO = getPageFromCategory(categoryId,page,size);

        postPageDTO.dtoList = postService.shortenPostList(postPageDTO.dtoList, contentLength);
        return postPageDTO;
    }

    public PostPageDTO getPageWithTag(Long tagId, int page, int size, int contentLength){
        PostPageDTO postPageDTO = getPageWithTag(tagId,page,size);

        postPageDTO.dtoList = postService.shortenPostList(postPageDTO.dtoList, contentLength);
        return postPageDTO;
    }

    public void deletePost(Long id) {
         restTemplate.delete(URL + ONE_URI, id);
    }

    public Long addPost(PostCreateDTO post) {
        return restTemplate.postForObject(URL,post,Long.class);
    }

    public void addTags(Long id, Long[] tags) {
        IdList ids = new IdList();
        ids.ids = Arrays.asList(tags);
        restTemplate.put(URL + ONE_URI + URL_ADD_TAGS, ids, id);
    }

    public void update(Long id, PostCreateDTO postCreateDTO) {
        restTemplate.put(URL + ONE_URI,postCreateDTO,id);
    }

    public void removeTags(Long id, Long[] tags) {
        IdList ids = new IdList();
        ids.ids = Arrays.asList(tags);
        restTemplate.put(URL + ONE_URI + URL_REMOVE_TAGS, ids, id);
    }

}