package cz.cvut.fit.vizeldim.dto.response;

public class CategoryDTO {
    public Long id;
    public String name;
    public String description;
}
